opts = detectImportOptions('SampledTraj.txt');
% opts = detectImportOptions('smoothedPath.txt');

opts = setvartype(opts,1:7,'double');
t = readtable('SampledTraj.txt', opts);
x= table2array(t(:,2));
y= table2array(t(:,3));
th = table2array(t(:,4));
Dist = table2array(t(:,1));

figure()
plot(x,y, 'b');
hold on

%%     T Curv;  // dAngle/dDist - brzina promjene kuta
% xx=smooth(x,50);
%  yy=smooth(y,50);
p = [x y];
 Angle=atan2(diff(p(:,2)),diff(p(:,1)));
 dAngle = abs(diff(Angle));
 dth = abs(diff(th));
%  dAngle = smooth(dAngle,10);
%  
figure()
plot(Dist,th, 'r')
hold on
plot(Dist(1:734),Angle, 'b')

legend('Misel', 'izracunato')
title ('Angle')
%  for i=1 : size(x)-1
%      diffx= (x(i+1)-x(i));
%      diffy = y(i+1)-y(i);
%      Angle(i)=atan2(diffy,diffx);
% %      pause
%  end
%  Angle = Angle';
% for i=1:size(Angle)-1
% dAngle(i) = abs(Angle (i+1)-Angle (i));
% dDist(i) = Dist(i+1)- Dist(i);
% end
dDist = 0.01;
Curv = dAngle./dDist;
Curv2 = dth./dDist;
figure()
plot(Dist(1:733),Curv)
hold on
plot(Dist(1:734),Curv2, 'r')
legend('Misel', 'izracunato')
title ('Curvature')
%plotanje samo ciljnih tocaka
%primjer 1: jednostavno.txt
%xx= [6.350000, 3.350000,  0.950000];
%yy= [3.150000, 5.450000, 5.95];
%primjer 2: tezi put

% xx= [6.350000, 3.350000, 2.650000, 2.550000, 0.950000];
% yy= [3.150000, 5.450000, 5.450000, 5.350000, 2.950000];

% xx= [6.350000, 3.350000, 2.650000, 2.550000, 0.950000, 0.950000];
% yy= [3.150000, 5.450000, 5.450000, 5.350000, 2.050000, 2.050000];


% primejr 3:
% xx= [8.350000, 9.150000, 9.150000, 9.050000, 8.350000, 8.250000, 8.150000, 7.550000, 7.350000, 6.450000];
% yy= [0.950000, 2.050000, 2.550000, 3.250000, 6.350000, 6.450000, 6.550000, 6.550000, 6.350000,2.650000 ];

%pr 4
% xx= [3.850000, 2.650000, 2.450000, 2.450000, 2.550000, 3.250000, 3.350000, 3.450000, 7.750000];
% yy= [5.750000, 5.450000, 5.250000, 4.850000, 4.150000, 1.050000, 0.950000, 0.850000, 0.750000 ];

% plot(xx,yy, 'r--', 'Linewidth', 0.8);
% 
% % legend('Smoothed Path', 'TWD*', 'start', 'goal');
% plot(x(1),y(1),'g*');
% hold on
% plot(x(end),y(end),'k*');
% hold on
% % text(x(1)+0.02,y(1)+0.02,'start');
% % text(x(end)+0.02,y(end)+0.02,'goal');
% 
% xlabel('x');
% ylabel('y');

