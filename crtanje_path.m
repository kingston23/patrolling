close all


opts = detectImportOptions('smoothedPath.txt');
opts = setvartype(opts,1:6,'double');
t = readtable('smoothedPath.txt', opts);

x= table2array(t(:,2));
y= table2array(t(:,3));


figure(1)
plot(x,y,'b.-')
xlabel('x [m]')
ylabel('y [m]')
title('trajectory')
axis equal tight