% calculate velocity profile for a path given numerically by a set of
% points

clear all, close all

% opts = detectImportOptions('smoothedPath1.txt');
opts = detectImportOptions('smoothedtraj23.txt');

opts = setvartype(opts,1:6,'double');
% t = readtable('smoothedPath1.txt', opts);
t = readtable('smoothedtraj23.txt', opts);

x= table2array(t(:,2));
y= table2array(t(:,3));
p = [x y];
% theta = table2array(t(1:end-1,4));
%p=ppp';

close all
% driving limitations
aradmax=0.1;    % m/s2
alinmax=0.5;    % m/s2
vmax=0.75;
wmax=1.75;
alphamax=1.75;
vrob(1)=0.07; vrob(2)=0.07; % intial and final velocity



% find out the number samples where is no motion
ind=   (diff(p(:,1)').^2+diff(p(:,2)'.^2) ~=0 ); numStillSample=sum(ind==0);
x=p(ind,1)';y=p(ind,2)'; % cut those samples out

resolucija = length(x)-1;
umin=0; umax=10;
uu=[umin:umax/resolucija:umax];
du=(umax-umin)/resolucija;

 xx=x; 
 yy=y;
 for i=1 : length(xx)-1
     diffx= (xx(i+1)-xx(i))
     diffy = yy(i+1)-yy(i)
     theta(i)=atan2(diffy,diffx)
     pause
 end

%  theta=atan2(diff(p(:,2)),diff(p(:,1)));
 
 % better derivatives (even better would be if spline of seccond order is fitted to the data)
 xxd= (-xx(1:end-2)+xx(3:end))/(2*du); xxd=[xxd(1),xxd,xxd(end)];
 yyd= (-yy(1:end-2)+yy(3:end))/(2*du); yyd=[yyd(1),yyd,yyd(end)];
 xxdd= (-xxd(1:end-2)+xxd(3:end))/(2*du); xxdd=[xxdd(1),xxdd,xxdd(end)];
 yydd= (-yyd(1:end-2)+yyd(3:end))/(2*du); yydd=[yydd(1),yydd,yydd(end)];


ds = sqrt(xxd.^2 + yyd.^2) * du; % distance between samples according u

kk = (xxd.*yydd-yyd.*xxdd) ./ ((xxd.^2+yyd.^2).^1.5) ;

kkabs=abs(kk);

ovinki=[]; % turn points
for i = 2:length(uu)-1 % Locate turn points
    if( kkabs(i)>kkabs(i-1) & kkabs(i)>kkabs(i+1) )
        ovinki=[ovinki , i];   % index of TPi
    end
end
ovinki=[1 ovinki resolucija+1];   % add request for start and end velocity

%%% zahteve za zacetne hitrosti
% ovinki=[1 ovinki resolucija+1];        % zacetne hitrosti 1 in resolucija+1
%%%


maksv=ones(length(ovinki),resolucija+1)*resolucija ;
for i=1:length(ovinki)
   maksv(i,ovinki(i))=sqrt(aradmax/kkabs(ovinki(i))); 
   if(maksv(i,ovinki(i))>vmax), maksv(i,ovinki(i))=vmax; end
   %new condition for wmax
   if(maksv(i,ovinki(i))*kkabs(ovinki(i))>wmax), maksv(i,ovinki(i))=wmax/kkabs(ovinki(i)); end
   

 
   %%% zahteve za zacetne hitrosti
   if i==1                                          % zacetne hitrosti
      if maksv(i,ovinki(i))<vrob(1);
         T=100;  disp('solution does not exist')
        % break;  % solution does not exist 
      else
         maksv(i,ovinki(i))=vrob(1);
      end
   elseif i==length(ovinki)
      if maksv(i,ovinki(i))<vrob(2);
         T=100; disp('solution does not exist')
         %break;   
      else
         maksv(i,ovinki(i))=vrob(2);
      end

   end
   %%%
      
   goF=1; goB=1;
   
   for j=0:resolucija+1
      %calculate back from TP
      if ovinki(i)-j-1 > 0  && goB
         
         arad = maksv(i,ovinki(i)-j)^2 * kkabs(ovinki(i)-j);
         if arad>aradmax, arad=aradmax;,end
         alin = alinmax * sqrt(1-(arad/aradmax)^2);
         if j==0 , alin=0; , end
         maksv(i,ovinki(i)-j-1) = sqrt( 2*alin*ds(ovinki(i)-j) + maksv(i,ovinki(i)-j)^2 );    % v^2=(2as)+vi^2
         if( maksv(i,ovinki(i)-j-1) >vmax ), maksv(i,ovinki(i)-j-1)=vmax; end
      end
      
      %calculate forward from TP
      if ovinki(i)+j+1 <= resolucija+1 && goF
         arad = maksv(i,ovinki(i)+j)^2 * kkabs(ovinki(i)+j);
         if arad>aradmax, arad=aradmax;,end
         alin = alinmax * sqrt(1-(arad/aradmax)^2);
         if j==0 , alin=0; , end
         maksv(i,ovinki(i)+j+1) = sqrt( 2*alin*ds(ovinki(i)+j) + maksv(i,ovinki(i)+j)^2 );
         if( maksv(i,ovinki(i)+j+1) >vmax ), maksv(i,ovinki(i)+j+1)=vmax; end
      
      else 
          if ~goB , break, end  % finish for loop if both goB and goF are false   
      
      end
   end
end
v=min(maksv);
dt=ds./v;       % ?? ds=vo*Ts + a*Ts^2/2 - od tu potegne� Ts
               % ?? sredna hitrost med vzorci v=(v1+v0)/2=vo+a*Ts/2 
               % ogranicenja su dobra, diskretizacija dt radi probleme jer
               % je aproksimacija
               %mozda probati s dt=2*ds./(v+vi)
dt=2*ds./([v(2:end) 0]+[v(1:end)]);


T=sum(dt(2:resolucija-1))  
D=sum(ds)


TT_mean=T+mean(dt)*0.5*numStillSample   % penalise still samples, they require some time for the robot to rotate ...
TT_min=T+min(dt)*numStillSample 


draw=1;

if draw
    tt_=cumsum([0,dt(1:resolucija)]); 
    uu_=uu(1:end);
    
    x2=xx(ovinki);
    y2=yy(ovinki);

    figure(1);
    plot(xx,yy,x2,y2,'o');  title('trajectory with TPs')
    axis equal;
    
    figure
    plot(uu,maksv,':',uu,v); 
    xlabel('$$u$$[ ]','interpreter','latex','FontSize',12)
    ylabel('$$v$$[m/s]','interpreter','latex','FontSize',12)
    %print -deps2 Gprofil_v_u2

    uup_=du./dt;  %??
    figure,plot(uu, uup_); 
    xlabel('$$u$$[ ]','interpreter','latex','FontSize',12)
    ylabel('$$\dot u $$[1/s]','interpreter','latex','FontSize',12)

    figure, plot(uu, v); 
    xlabel('$$u$$[ ]','interpreter','latex','FontSize',12)
    ylabel('$$v$$[m/s]','interpreter','latex','FontSize',12)

    figure, plot(tt_,v)
    xlabel('$$t$$[s]','interpreter','latex','FontSize',12)
    ylabel('$$v$$[m/s]','interpreter','latex','FontSize',12)

    figure, plot(tt_,uu_), xlabel('t'),ylabel('u')
    xlabel('$$t$$[s]','interpreter','latex','FontSize',12)
    ylabel('$$u$$[ ]','interpreter','latex','FontSize',12)
    
        figure, plot(tt_,v.*kk)
    xlabel('$$t$$[s]','interpreter','latex','FontSize',12)
    ylabel('$$\omega$$[1/s]','interpreter','latex','FontSize',12)


     figure;
 plot(uu,kk,uu(ovinki),kk(ovinki),'o'); title('Curvature with marked TPs')
 
         figure, plot(diff(v.*kk)./diff(tt_))
         hold on
         plot(diff(v.*kk)./abs(diff(v.*kk))*alphamax,'m*')
    xlabel('index','interpreter','latex','FontSize',12)
    ylabel('alpha','interpreter','latex','FontSize',12)

         figure, plot(diff(v)./diff(tt_))
         hold on
         plot(diff(v)./abs(diff(v))*alinmax,'m*')
    xlabel('index','interpreter','latex','FontSize',12)
    ylabel('a','interpreter','latex','FontSize',12)

end

%spremanje
a(:,3) = y;
a(:,2) = x;
a(:,1)=tt_;
a(:,4)=theta;
a(:,5)=v;
a(:,6)=v.*kk;

% vektore xx i v   dobiješ od OptimizeVelProfileSimple_... fajla
% goal=[4 8];%Umap
% goal=[3 8.5];%Smap
% goal=[8 7];%ozki
goal=[17.1 17.1]; %rand obst2
distx=goal(1)-xx(end);  

disty=goal(2)-yy(end); 

 

notTraveledDist=sqrt(distx^2+disty^2);

dx=diff(p(:,1));
dy=diff(p(:,2));
D=sum(sqrt(dx.^2+dy.^2))
costDist=D+notTraveledDist

costTime=T+notTraveledDist/v(end)


% figure(1);
% plot(xx,yy,x2,y2,'o');  title('trajectory with TPs')
% axis equal;
% 
% 
% figure(2);
% plot(uu,maksv,':',uu,v); title('velocity profilers for TPs')
% 
% figure(3);
% plot(uu,kk,uu(ovinki),kk(ovinki),'o'); title('Curvature with marked TPs')
% 
% 
% 
% figure(5)
% plot(tt_,uu_), xlabel('t'),ylabel('u')



writematrix(a,'smoothed.csv') 




