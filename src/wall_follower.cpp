#include "ros/ros.h"
#include "std_msgs/String.h"
#include <math.h>
#include <sstream>
#include <vector>

#include<sensor_msgs/LaserScan.h>
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/Twist.h"
#include <tf/transform_listener.h>
#include "nav_msgs/Odometry.h"
#include <numeric>   	// for std::partial_sum
#include <fstream>

#include <twdplanner/Path.h>

#include "nodelet_path_smoothing/OutputTraj.h"
#include "nodelet_talker/Patroling.h"
#include <pathtwdlistener/FilteredPath.h>
#include <pathtwdlistener/FilteredTWDPath.h>


//________________________________________________________________________________________________________________________________________________
//
// NAPOMENA1: kod rezerviranja memorije gdje se koriste vektori bi moglo biti problema ako je ulazna poruka s vise podataka nego sto je rezervirano
// NAPOMENA2: kad se koristi patroling postaviti zastavicu patroling = 1, inace patroling = 0 ili napraviti poruku koja ima podatak koliko puta
// se ponavljaju zadani ciljevi (PROBLEM: ne procita broj laps ako nije u while(1) -> potrebno popraviti u talker.cpp)
//
//________________________________________________________________________________________________________________________________________________


double odom_v,odom_w;
int trackingON = 1; // 1 - ukljucivanje pracenja trajektroije,  0 - iskuljcivanje pracenja trajektroije
int patroling = 0;  // 1 - ukljucen patroling, 0 - iskljucena patroling
int obstacle = 0;
int laps;
int twdflag = 0;
int init = 0;
int revpath = 0;
int firststart = 0;
double min_range = 15.2;
double vrob0;
double wrob0;
int driving_lim = 1; // 0 - pioner, 1 -husky
double aradmax, alinmax, alfamax, vmax, wmax;
//tracking
double xc,yc,thc,vc,wc;
double tfx, tfy, yaw;

double nextX, nextY;

struct P_point{
	double  x, y;
};

P_point p;
std::vector<P_point> ppath;

struct T_point{
	double  x, y, th, v, w, curv;
};

  T_point tr;

  std::vector<T_point> a;
  std::vector<T_point> ref;
  std::vector<double> t;

class SubscriberTWD{
  protected:
  // Our NodeHandle
  geometry_msgs::Twist vel;
  geometry_msgs::PoseStamped goal;
  ros::NodeHandle nh_;
  ros::Time present;
  tf::StampedTransform transform;
  tf::TransformListener tf_listener;

public:
	ros::Subscriber pathtwd_sub;
  ros::Subscriber twdflag_sub;
  ros::Subscriber sub;
  ros::Subscriber gridMap_sub;
  ros::Subscriber patrollingPath_sub;
  //ros::Subscriber odom_sub;
  //ros::Subscriber laps_sub;

  ros::Publisher vel_pub;
  ros::Publisher TWDgoalpub;
	
    SubscriberTWD(ros::NodeHandle n) :
      nh_(n)
  {
  pathtwd_sub = nh_.subscribe<nodelet_path_smoothing::OutputTraj>("/output_path/SmoothedTrajPub",1, &SubscriberTWD::pathCallback, this);

  //sub = nh_.subscribe("odom", 10, &SubscriberTWD::chatterCallback, this); //stage
  sub = nh_.subscribe("/odom", 10, &SubscriberTWD::chatterCallback, this); // za patroling primjer DUV gazebo
  //gridMap_sub = nh_.subscribe("/grid_map_simple_demo/scan", 10, &SubscriberTWD::gridMapCallback, this); // za patroling primjer DUV
  gridMap_sub = nh_.subscribe("/base_scan", 10, &SubscriberTWD::gridMapCallback, this); // base_scan -> stage /scan ->gazeboo 

  }

  //void lapsCallback(const nodelet_talker::PatrolingConstPtr& Msg);
  void gridMapCallback(const sensor_msgs::LaserScan::ConstPtr& msg);
  //void FilteredPathPatrollingCallback(const pathtwdlistener::FilteredPathConstPtr& pMsg);
  void chatterCallback(const nav_msgs::Odometry::ConstPtr& msg);
  //void TWDgoalPublisher();
};


int main(int argc, char** argv)
{
  
  ros::init(argc, argv, "drivetraj");
  ros::NodeHandle nh;


  ros::Rate rate(10.0);

  ppath.reserve(10000); 
  ppath.clear();
  a.reserve(10000); 
  a.clear();
  t.reserve(10000); 
  t.clear();
  SubscriberTWD subTWD(nh);
 	//ros::Subscriber pathtwd_sub = nh.subscribe<nodelet_path_smoothing::OutputTraj>("/output_path/SmoothedPathPub",1, pathCallback);
  //printf("prosao pathtwd_sub");
  //ros::Subscriber sub = nh.subscribe("odom", 1000, SubscriberTWD::chatterCallback);
  //ros::Duration(5).sleep();

  while (nh.ok()) {
      ros::spinOnce(); 

  //drawing in rviz	  
      // if(trackingON == 1)
      // {
         //subTWD.kanayamaTracking();	
      // }
    
      rate.sleep();
    }
  return 0;
}


void SubscriberTWD::kanayamaTracking(){
double dt=0.1;

// if (driving_lim == 0){ 
// // driving limitations pioner
double aradmax=0.1;    // m/s2
double alinmax=0.5;    // m/s2
double alfamax=1.75; //double alfamax=100*M_PI/180;
double vmax=0.75;
double wmax = 1.75; //double wmax=100*M_PI/180;
double amax=0.5;
//double vrob0=0.07;
// //double vrob1=0.07; //intial and final velocity
// }
// else if (driving_lim ==1){    
// driving limitations for husky
// double aradmax=0.08;    // m/s2
// double alinmax=0.3; // m/s2
// double vmax=0.5; 
// double wmax=0.5236; //30*M_PI/180; 
// double alfamax=0.8727; //50*M_PI/180; 
//} 



        xc=a[0].x;
        yc=a[0].y;
        thc=a[0].th;
        if (init==0){
          vc=0.;//pocetna brzina
          wc=0;
          vrob0=0.07;
        }
        // else{
        //   vc = vrob0;
        //   wc = wrob0;
        // }

        //goal coordinates
        T_point G=a.back();
        T_point actual;
        double xG=xc-0.1;
        double yG=yc;
        double thG=G.th;
        if (thG>M_PI){
            thG=thG-2*M_PI;
        }
        if (thG<-M_PI){
            thG=thG+2*M_PI;
        }
        // kanayama: some coefficient
        double k_x = 0.25;
        double k_y = 0.25;
        double k_theta = 5 * sqrt(k_y);
        double ex,ey,eth;
        double vs,ws,vt,wt,xt,yt,tht;
        double alfac,ac;

        int kk=0;
        int jj=-1;
        int krug = 0;


  FILE *logfile;
  logfile = fopen("odvozena","w");
  fclose(logfile);

  double pitch, roll;
	//ros::NodeHandle n;
	ros::Rate rate(10.0);

  vel_pub=nh_.advertise<geometry_msgs::Twist>("cmd_vel", 1);
  
  present = ros::Time::now();
  double duration;

  while (nh_.ok()) {
    ros::spinOnce(); 
	  try{
//        tf_listener.lookupTransform("map", "Pioneer3DX/base_link", ros::Time(0), transform);//robot_0/
    tf_listener.lookupTransform("map", "base_link", ros::Time(0), transform);//odom za patroling, a inace world_frame=map za pioner 
    //   tf_listener.lookupTransform("map", "base_link", ros::Time(0), transform); // za pionera
	  }
	  catch (tf::TransformException ex){
		  ROS_ERROR("%s",ex.what());
	  }
    transform.getBasis().getRPY(roll, pitch, yaw);
	  tfx=transform.getOrigin().x();
	  tfy=transform.getOrigin().y();
	  printf("\n map to base_link: x [%f], y [%f] and th [%f] deg\n", tfx, tfy, yaw*180/M_PI);

//tracking
  duration = (ros::Time::now() - present).toSec();
  printf("duration=%f\n",duration);
  printf("kk=%d\n",kk);
  printf("(xG,yG,thG)=(%f,%f,%f), current (xc,yc,thc)=(%f,%f,%f)\n",xG,yG,thG,xc,yc,thc);
  printf("revpath flag: %d", revpath);
        
          if ((fabs(fabs(xc)-fabs(xG))>0.08) && (fabs(fabs(yc)-fabs(yG))>0.08){ // || (std::min(fabs(thc-thG),std::min(fabs(thc-thG-2*M_PI),fabs(thc-thG+2*M_PI)))>25*M_PI/180) || (fabs(vc)>amax*dt) || (fabs(wc)>alfamax*dt)){
            //if((fabs(xc-xG)>0.08) || (fabs(yc-yG)>0.08)){ 
            jj=jj+1;
            while ((kk+1<a.size()) && (duration>=t[kk+1])){
                kk=kk+1;
            }
            vt=a[kk].v;
            wt=a[kk].w;
            xt=a[kk].x;
            yt=a[kk].y;
            tht=a[kk].th;
            ex=(xt-xc)*cos(thc)+(yt-yc)*sin(thc);
            ey=-(xt-xc)*sin(thc)+(yt-yc)*cos(thc);
            eth=tht-thc;
            // if (vt<0) {//kljucan dio za rikverc
            //     eth=thc-tht;
            //     revpath = 1;
            // }
            if (eth>M_PI){
                eth=eth-2*M_PI;
            }
            if (eth<-M_PI){
                eth=eth+2*M_PI;
            }
//                corrected control
            ws = wt + vt * (k_y * ey + k_theta * sin(eth));
            alfac=alfamax*boost::math::sign(ws-wc);
            if (fabs((ws-wc)/dt)<fabs(alfac)){
                alfac=(ws-wc)/dt;
            }
            wc=wc+alfac*dt;
            if (wc>wmax){
                wc=wmax;
            }
            if (wc<-wmax){
                wc=-wmax;
            }
//                %corrected control
            vs = vt * cos(eth) + k_x * ex;
            ac=amax*boost::math::sign(vs-vc);
            if (fabs((vs-vc)/dt)<fabs(ac)){
                ac=(vs-vc)/dt;
            }
            printf("ac=%f vs=%f vc=%f\n",ac,vs,vc);
            vc=vc+ac*dt;
            if (vc>vmax){
                vc=vmax;
            }
            if (vc<-1*vmax){
                vc=-1*vmax;
            }

        // }else{
        //   //if(patroling == 1){
        //     //if((fabs(xc-xG)<0.1) || (fabs(yc-yG)<0.1)){
        //     while(fabs(vc)>amax*dt){
        //       ac=amax*boost::math::sign(vs-vc);
        //       vc=vc+ac*dt;
        //     }
            //logfile = fopen("odvozena","a");
            //fprintf(logfile,"%f %f %f %f %f %f %f %f\n", tfx, tfy, yaw*180/M_PI,vc,wc,odom_v,odom_w,duration);
            //fclose(logfile);
            //break;
          //}
        //}
            actual.x=xc;
            actual.y=yc;
            actual.v=vc;
            actual.th=thc;
            actual.w=wc;
            ref.push_back(actual);

            //po stvarnom gibanju
            xc=tfx;
            yc=tfy;
            thc=yaw;
            // if (init==1){
            //   vrob0 = vc;
            //   wrob0 = wc;
            // }

        }

    vel.linear.x = vc;
    vel.linear.y = 0.;
    vel.angular.z = wc;
    printf("setpoint: (%f,%f)\n",vc,wc);
    logfile = fopen("odvozena","a");
	  fprintf(logfile,"%f %f %f %f %f %f %f %f\n", tfx, tfy, yaw*180/M_PI,vc,wc,odom_v,odom_w,duration);
//	  fprintf(logfile,"%f %f %f %f %f %f %f %f\n", xc, yc, thc,vc,wc,odom_v,odom_w,duration);
    fclose(logfile);
    vel_pub.publish(vel);
    
	  rate.sleep();

  if((fabs(xc-xG)<0.08) && (fabs(yc-yG)<0.08) && revpath == 1){
    break;
  }

  }
} 



void SubscriberTWD::gridMapCallback(const sensor_msgs::LaserScan::ConstPtr& msg){
  for(int i_LS = 320; i_LS < 400; i_LS++){ //gazebo od 350 do 370
    //for(int i_LS = 0; i_LS< msg->ranges.size(); i_LS++){
    if (msg->ranges[i_LS] < min_range && msg->ranges[i_LS] > 0.225){
      obstacle = 1;
      //ROS_INFO("obstacle laser point number %d, length: %f", i_LS, msg->ranges[i_LS]);
      break;

    } else if(msg->ranges[360] > min_range){
      obstacle = 0;
      if (firststart == 0){
        revpath = 1;
        firststart = 1;
      }
      //ROS_INFO("obstacle laser ZEROO");
      //twdflag = 0;
    }
  }
}



void SubscriberTWD::chatterCallback(const nav_msgs::Odometry::ConstPtr& msg)
//void SubscriberTWD::chatterCallback(const nav_msgs::OdometryConstPtr& msg)
{
  //ROS_INFO("Seq: [%d]", msg->header.seq);
  //ROS_INFO("Position-> x: [%f], y: [%f], z: [%f]", msg->pose.pose.position.x,msg->pose.pose.position.y, msg->pose.pose.position.z);
  //ROS_INFO("Orientation-> x: [%f], y: [%f], z: [%f], w: [%f]", msg->pose.pose.orientation.x, msg->pose.pose.orientation.y, msg->pose.pose.orientation.z, msg->pose.pose.orientation.w);
  //ROS_INFO("Vel-> Linear: [%f], Angular: [%f]", msg->twist.twist.linear.x,msg->twist.twist.angular.z);
  odom_v=msg->twist.twist.linear.x;
  odom_w=msg->twist.twist.angular.z;
  }


