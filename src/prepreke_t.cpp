#include <acado_optimal_control.hpp>
#include <acado_gnuplot.hpp>

using namespace ACADO;

int main( ){



    // INTRODUCE THE VARIABLES:
    // -------------------------
    const long double PI = 3.141592653589793238L;

    DifferentialState x;    // Position in x-direction
    DifferentialState y;    // Position in y-direction
    DifferentialState theta;  // Orientation
    DifferentialState v;    // Linear velocity
    DifferentialState w;    // Angular velocity

   
    Parameter T;

    Control a,alfa; // Control input (a; alfa )


    DifferentialEquation  f( 0.0, T );

    // DEFINE A DIFFERENTIAL EQUATION:
    // -------------------------------

    f << dot(x) == cos(theta)*v;
    f << dot(y) == sin(theta)*v;
    f << dot(theta) == w;
    f << dot(v) == a;
    f << dot(w) == alfa;

 
    // DEFINE AN OPTIMAL CONTROL PROBLEM:
    // ----------------------------------
    OCP ocp1(0, T, 150);
//    OCP ocp2(0, T, 150);
    OCP ocp3(0, T, 150);
//    OCP ocp4(0, T, 150);
//    OCP ocp5(0, T, 150);
//    OCP ocp6(0, T, 150);

    ocp1.minimizeMayerTerm( T );
    ocp1.subjectTo( f );
//    ocp2.minimizeMayerTerm( T );
//    ocp2.subjectTo( f );
    ocp3.minimizeMayerTerm( T );
    ocp3.subjectTo( f );
//    ocp4.minimizeMayerTerm( T );
//    ocp4.subjectTo( f );
//    ocp5.minimizeMayerTerm( T );
//    ocp5.subjectTo( f );
//    ocp6.minimizeMayerTerm( T );
//    ocp6.subjectTo( f );

    ocp1.subjectTo( AT_START, x ==  30.45 );
    ocp1.subjectTo( AT_START, y ==  8.65 );
    ocp1.subjectTo( AT_START, theta ==  PI/2 );
    ocp1.subjectTo( AT_START, v ==  0.0 );
    ocp1.subjectTo( AT_START, w ==  0.0 );

//    ocp2.subjectTo( AT_START, x ==  30.55 );
//    ocp2.subjectTo( AT_START, y ==  6.4 );
//    ocp2.subjectTo( AT_START, theta ==  PI/2 );
//    ocp2.subjectTo( AT_START, v ==  0.6 );
//    ocp2.subjectTo( AT_START, w ==  0.0 );

    ocp3.subjectTo( AT_START, x ==  6.4 );
    ocp3.subjectTo( AT_START, y ==  3.1 );
    ocp3.subjectTo( AT_START, theta ==  -PI/2 );
    ocp3.subjectTo( AT_START, v ==  0.0 );
    ocp3.subjectTo( AT_START, w ==  0.0 );

//    ocp4.subjectTo( AT_START, x ==  2.0 ); //30.45
//    ocp4.subjectTo( AT_START, y ==  7.0 );
//    ocp4.subjectTo( AT_START, theta ==  PI );
//    ocp4.subjectTo( AT_START, v ==  0.6 );
//    ocp4.subjectTo( AT_START, w ==  0.0 );

//    ocp5.subjectTo( AT_START, x ==  23.5 );
//    ocp5.subjectTo( AT_START, y ==  5.7 );
//    ocp5.subjectTo( AT_START, theta ==  -PI );
//    ocp5.subjectTo( AT_START, v ==  0.6 );
//    ocp5.subjectTo( AT_START, w ==  0.0 );

//    ocp6.subjectTo( AT_START, x ==  21.35 );
//    ocp6.subjectTo( AT_START, y ==  6.1 );//6.1
//    ocp6.subjectTo( AT_START, theta ==  PI/2 );
//    ocp6.subjectTo( AT_START, v ==  0.6 );
//    ocp6.subjectTo( AT_START, w ==  0.0 );


    ocp1.subjectTo( AT_END  , x == 29. );
    ocp1.subjectTo( AT_END  , y ==  8.65 );
    ocp1.subjectTo( AT_END  , theta == PI );
    ocp1.subjectTo( AT_END  , v ==  0.0 );
    ocp1.subjectTo( AT_END  , w == 0.0 );

//    ocp2.subjectTo( AT_END  , x == 30.45 );
//    ocp2.subjectTo( AT_END  , y ==  8.65 );
//    ocp2.subjectTo( AT_END  , theta == PI/2 );
//    ocp2.subjectTo( AT_END  , v ==  0.0 );
//    ocp2.subjectTo( AT_END  , w == 0.0 );

    ocp3.subjectTo( AT_END  , x == 2. );
    ocp3.subjectTo( AT_END  , y ==  7. );
    ocp3.subjectTo( AT_END  , theta == PI );
    ocp3.subjectTo( AT_END  , v ==  0.0 );
    ocp3.subjectTo( AT_END  , w == 0.0 );

//    ocp4.subjectTo( AT_END  , x == 2.0 );
//    ocp4.subjectTo( AT_END  , y ==  2.0 );
//    ocp4.subjectTo( AT_END  , theta == -PI/2 );
//    ocp4.subjectTo( AT_END  , v ==  0.0 );
//    ocp4.subjectTo( AT_END  , w == 0.0 );

//    ocp5.subjectTo( AT_END  , x == 21.35 );
//    ocp5.subjectTo( AT_END  , y ==  6.1 );
//    ocp5.subjectTo( AT_END  , theta == -3*PI/2 );
//    ocp5.subjectTo( AT_END  , v ==  0.6 );
//    ocp5.subjectTo( AT_END  , w == 0.0 );
//    
//    ocp6.subjectTo( AT_END  , x == 21.35 );
//    ocp6.subjectTo( AT_END  , y ==  8.55 );
//    ocp6.subjectTo( AT_END  , theta == 0 );
//    ocp6.subjectTo( AT_END  , v ==  0.0 );
//    ocp6.subjectTo( AT_END  , w == 0.0 );


    ocp1.subjectTo(0 <= v <= 0.75);     // Path constraint on speed
    ocp1.subjectTo(-100*PI/180 <= w <= 100*PI/180 );
    ocp1.subjectTo(-0.5 <= a <= 0.5);
    ocp1.subjectTo(-100*PI/180 <= alfa <= 100*PI/180 );

//    ocp2.subjectTo(0 <= v <= 0.6);
//    ocp2.subjectTo(-100*PI/180 <= w <= 100*PI/180 );
//    ocp2.subjectTo(-0.3 <= a <= 0.3);
//    ocp2.subjectTo(-100*PI/180 <= alfa <= 100*PI/180 );

    ocp3.subjectTo(-0.75 <= v <= 0.75);
    ocp3.subjectTo(-100*PI/180 <= w <= 100*PI/180 );
    ocp3.subjectTo(-0.5 <= a <= 0.5);
    ocp3.subjectTo(-100*PI/180 <= alfa <= 100*PI/180 );

//    ocp4.subjectTo(-0.6 <= v <= 0.6);
//    ocp4.subjectTo(-100*PI/180 <= w <= 100*PI/180 );
//    ocp4.subjectTo(-0.3 <= a <= 0.3);
//    ocp4.subjectTo(-100*PI/180 <= alfa <= 100*PI/180 );

//    ocp5.subjectTo(-0.6 <= v <= 0.6);
//    ocp5.subjectTo(-100*PI/180 <= w <= 100*PI/180 );
//    ocp5.subjectTo(-0.3 <= a <= 0.3);
//    ocp5.subjectTo(-100*PI/180 <= alfa <= 100*PI/180 );

//    ocp6.subjectTo(-0.6 <= v <= 0.6);
//    ocp6.subjectTo(-100*PI/180 <= w <= 100*PI/180 );
//    ocp6.subjectTo(-0.3 <= a <= 0.3);
//    ocp6.subjectTo(-100*PI/180 <= alfa <= 100*PI/180 );

    ocp1.subjectTo(0 <= T <= 5); 
//    ocp2.subjectTo(0 <= T <= 7); 
    ocp3.subjectTo(0 <= T <= 13); 
//    ocp4.subjectTo(0 <= T <= 13); 
//    ocp5.subjectTo(0 <= T <= 7); 
//    ocp6.subjectTo(0 <= T <= 7); 

//    ocp1.subjectTo(30.3 <= x <= 36.3); ocp1.subjectTo(5.8 <= y <= 6.4);
//    ocp2.subjectTo(30.3 <= x <= 30.6); ocp2.subjectTo(6.4 <= y <= 8.8);
//    ocp3.subjectTo(3. <= x <= 8.); ocp3.subjectTo(2. <= y <= 6.);//ocp3.subjectTo(6.4 <= y <= 8.65);
//    ocp4.subjectTo(23.5 <= x <= 30.6); ocp4.subjectTo(5.4 <= y <= 6.4);
//    ocp5.subjectTo(20.7 <= x <= 23.5); ocp5.subjectTo(5.4 <= y <= 6.1);
//    ocp6.subjectTo(21.3 <= x <= 21.6); ocp6.subjectTo(5.7 <= y <= 8.9);



    // VISUALIZE THE RESULTS IN A GNUPLOT WINDOW:
    // ------------------------------------------
    GnuplotWindow window1, window2, window3, window4, window5, window6;
   
    window1.addSubplot( x, y, "Trajektorija", "x1 [m]", "y1 [m]", PM_LINES,28.,35.,5.,11.);
    //window1.addSubplot( x, "Pozicija u x-smjeru", "Vrijeme [s]", "x1 [m]");
    //window1.addSubplot( y, "Pozicija u y-smjeru", "Vrijeme [s]", "y1 [m]");
    //window1.addSubplot( theta, "Orijentacija", "Vrijeme [s]", "theta1 [rad]");
    window1.addSubplot( v, "Linearna brzina", "Vrijeme [s]", "v1 [m/s]");
    window1.addSubplot( w, "Kutna brzina", "Vrijeme [s]", "omega1 [rad/s]");
    //window1.addSubplot( a, "Linearna akcelercija", "Vrijeme [s]", "a1 [m/s^2]");
    //window1.addSubplot( alfa, "Kutna akceleracija", "Vrijeme [s]", "alfa1 [rad/s^2]");
 
    window2.addSubplot( x, y, "Trajektorija", "x2 [m]", "y2 [m]", PM_LINES,1.,7.,1.,8.);
    //window2.addSubplot( x, "Pozicija u x-smjeru", "Vrijeme [s]", "x2 [m]");
    //window2.addSubplot( y, "Pozicija u y-smjeru", "Vrijeme [s]", "y2 [m]");
    //window2.addSubplot( theta, "Orijentacija", "Vrijeme [s]", "theta2 [rad]");
    window2.addSubplot( v, "Linearna brzina", "Vrijeme [s]", "v2 [m/s]");
    window2.addSubplot( w, "Kutna brzina", "Vrijeme [s]", "omega2 [rad/s]");
    //window1.addSubplot( a, "Linearna akcelercija", "Vrijeme [s]", "a2 [m/s^2]");
    //window1.addSubplot( alfa, "Kutna akceleracija", "Vrijeme [s]", "alfa2 [rad/s^2]");

    window3.addSubplot( x, y, "Trajektorija", "x3 [m]", "y3 [m]", PM_LINES,1.,8.,1.,8.);
    //window3.addSubplot( x, "Pozicija u x-smjeru", "Vrijeme [s]", "x3 [m]");
    //window3.addSubplot( y, "Pozicija u y-smjeru", "Vrijeme [s]", "y3 [m]");
    //window3.addSubplot( theta, "Orijentacija", "Vrijeme [s]", "theta3 [rad]");
    window3.addSubplot( v, "Linearna brzina", "Vrijeme [s]", "v3 [m/s]");
    window3.addSubplot( w, "Kutna brzina", "Vrijeme [s]", "omega3 [rad/s]");
    //window3.addSubplot( a, "Linearna akcelercija", "Vrijeme [s]", "a3 [m/s^2]");
    //window3.addSubplot( alfa, "Kutna akceleracija", "Vrijeme [s]", "alfa3 [rad/s^2]");

    window4.addSubplot( x, y, "Trajektorija", "x4 [m]", "y4 [m]", PM_LINES,1.,7.,1.,8.);
    //window4.addSubplot( x, "Pozicija u x-smjeru", "Vrijeme [s]", "x4 [m]");
    //window4.addSubplot( y, "Pozicija u y-smjeru", "Vrijeme [s]", "y4 [m]");
    //window4.addSubplot( theta, "Orijentacija", "Vrijeme [s]", "theta4 [rad]");
    window4.addSubplot( v, "Linearna brzina", "Vrijeme [s]", "v4 [m/s]");
    window4.addSubplot( w, "Kutna brzina", "Vrijeme [s]", "omega4 [rad/s]");
    //window4.addSubplot( a, "Linearna akcelercija", "Vrijeme [s]", "a4 [m/s^2]");
    //window4.addSubplot( alfa, "Kutna akceleracija", "Vrijeme [s]", "alfa4 [rad/s^2]");

    window5.addSubplot( x, y, "Trajektorija", "x5 [m]", "y5 [m]", PM_LINES,20.7,23.5,5.4,6.1);
    //window5.addSubplot( x, "Pozicija u x-smjeru", "Vrijeme [s]", "x5 [m]");
    //window5.addSubplot( y, "Pozicija u y-smjeru", "Vrijeme [s]", "y5 [m]");
    //window5.addSubplot( theta, "Orijentacija", "Vrijeme [s]", "theta5 [rad]");
    window5.addSubplot( v, "Linearna brzina", "Vrijeme [s]", "v5 [m/s]");
    window5.addSubplot( w, "Kutna brzina", "Vrijeme [s]", "omega5 [rad/s]");
    //window5.addSubplot( a, "Linearna akcelercija", "Vrijeme [s]", "a5 [m/s^2]");
    //window5.addSubplot( alfa, "Kutna akceleracija", "Vrijeme [s]", "alfa5 [rad/s^2]");

    window6.addSubplot( x, y, "Trajektorija", "x6 [m]", "y6 [m]", PM_LINES,21.3,21.6,6.1,8.9);
    //window6.addSubplot( x, "Pozicija u x-smjeru", "Vrijeme [s]", "x6 [m]");
    //window6.addSubplot( y, "Pozicija u y-smjeru", "Vrijeme [s]", "y6 [m]");
    //window6.addSubplot( theta, "Orijentacija", "Vrijeme [s]", "theta6 [rad]");
    window6.addSubplot( v, "Linearna brzina", "Vrijeme [s]", "v6 [m/s]");
    window6.addSubplot( w, "Kutna brzina", "Vrijeme [s]", "omega6 [rad/s]");
    //window6.addSubplot( a, "Linearna akcelercija", "Vrijeme [s]", "a6 [m/s^2]");
    //window6.addSubplot( alfa, "Kutna akceleracija", "Vrijeme [s]", "alfa6 [rad/s^2]");
    
    // DEFINE AN OPTIMIZATION ALGORITHM AND SOLVE THE OCP:
    // ---------------------------------------------------
//    OptimizationAlgorithm algorithm1(ocp1);
//    algorithm1 << window1;
//    //algorithm1 << window;
//    algorithm1.solve();
//    algorithm1.getDifferentialStates("states1.csv");
//    algorithm1.getParameters("parameters1.csv");
//    algorithm1.getControls("controls1.csv");

//    OptimizationAlgorithm algorithm2(ocp2);
//    algorithm2 << window2;
//    //algorithm2 << window;
//    algorithm2.solve();
//    algorithm2.getDifferentialStates("states2.csv");
//    algorithm2.getParameters("parameters2.csv");
//    algorithm2.getControls("controls2.csv");
//    //clearAllStaticCounters();

    OptimizationAlgorithm algorithm3(ocp3);
    algorithm3 << window3;
    //algorithm3 << window;
    algorithm3.solve();
    algorithm3.getDifferentialStates("states3.csv");
    algorithm3.getParameters("parameters3.csv");
    algorithm3.getControls("controls3.csv");
//    clearAllStaticCounters();

//    OptimizationAlgorithm algorithm4(ocp4);
//    algorithm4 << window4;
//    //algorithm4 << window;
//    algorithm4.solve();
//    algorithm4.getDifferentialStates("states4.csv");
//    algorithm4.getParameters("parameters4.csv");
//    algorithm4.getControls("controls4.csv");
//    //clearAllStaticCounters();

//    OptimizationAlgorithm algorithm5(ocp5);
//    algorithm5 << window5;
//    //algorithm5 << window;
//    algorithm5.solve();
//    algorithm5.getDifferentialStates("states5.csv");
//    algorithm5.getParameters("parameters5.csv");
//    algorithm5.getControls("controls5.csv");
//    //clearAllStaticCounters();

//    OptimizationAlgorithm algorithm6(ocp6);
//    algorithm6 << window6;
//    //algorithm6 << window;
//    algorithm6.solve();
//    algorithm6.getDifferentialStates("states6.csv");
//    algorithm6.getParameters("parameters6.csv");
//    algorithm6.getControls("controls6.csv");
//    //clearAllStaticCounters();


    return 0;
}



