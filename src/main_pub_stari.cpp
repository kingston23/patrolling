#include "ros/ros.h"
#include "std_msgs/String.h"

#include <sstream>
#include <vector>
#include "geometry_msgs/Twist.h"
#include <tf/transform_listener.h>
#include "nav_msgs/Odometry.h"
#include <numeric>   	// for std::partial_sum
#include <fstream>

#include "nodelet_path_smoothing/OutputTraj.h"
#include "nodelet_talker/Patroling.h"

//________________________________________________________________________________________________________________________________________________
//
// NAPOMENA1: kod rezerviranja memorije gdje se koriste vektori bi moglo biti problema ako je ulazna poruka s vise podataka nego sto je rezervirano
// NAPOMENA2: kad se koristi patroling postaviti zastavicu patroling = 1, inace patroling = 0 ili napraviti poruku koja ima podatak koliko puta
// se ponavljaju zadani ciljevi (PROBLEM: ne procita broj laps ako nije u while(1) -> potrebno popraviti u talker.cpp)
//
//________________________________________________________________________________________________________________________________________________


double odom_v,odom_w;
int trackingON = 1; // 1 - ukljucivanje pracenja trajektroije,  0 - iskuljcivanje pracenja trajektroije
int patroling = 1;  // 1 - ukljucen patroling, 0 - iskljucena patroling
int laps;
int init = 0;
double vrob0;
double wrob0;


struct T_point{
	double  x, y, th, v, w, curv;
};

  T_point tr;

  std::vector<T_point> a;
  std::vector<T_point> ref;
  std::vector<double> t;

class SubscriberTWD{
  protected:
  // Our NodeHandle
  geometry_msgs::Twist vel;
  ros::NodeHandle nh_;
  ros::Time present;

  tf::StampedTransform transform;
  tf::TransformListener tf_listener;

public:
	ros::Subscriber pathtwd_sub;
  ros::Subscriber sub;
  ros::Subscriber laps_sub;

  ros::Publisher vel_pub;

	  SubscriberTWD(ros::NodeHandle n) :
      nh_(n)
  {
  pathtwd_sub = nh_.subscribe<nodelet_path_smoothing::OutputTraj>("/output_path/SmoothedTrajPub",1, &SubscriberTWD::pathCallback, this);
  sub = nh_.subscribe("odom", 1000, &SubscriberTWD::chatterCallback, this);
  laps_sub = nh_.subscribe<nodelet_talker::Patroling>("/input_talker/msg_laps",1, &SubscriberTWD::lapsCallback, this);
  }

  void lapsCallback(const nodelet_talker::PatrolingConstPtr& Msg);
  void pathCallback(const nodelet_path_smoothing::OutputTrajConstPtr& pMsg);
  void chatterCallback(const nav_msgs::Odometry::ConstPtr& msg);
  void kanayamaTracking();
};



T_point kinmod(std::vector<T_point> ref){
    //delay of 3 time steps on p3dx
    double v, w;
    double x,y,th;
    double dt=0.1;
    T_point actual;
    int delaydt=0;
    int i=ref.size()-1;
      v=ref[i-delaydt].v;
      w=ref[i-delaydt].w;
			if(fabs(w)>0)//W_TOLERANCE)
			{
				th=ref[i].th+w*dt;
				x=ref[i].x+v/w*(sin(th)-sin(ref[i].th));
				y=ref[i].y-v/w*(cos(th)-cos(ref[i].th));
			}else{
				th=ref[i].th;
				x=ref[i].x+v*dt*cos(th);
				y=ref[i].y+v*dt*sin(th);
			}
    if (th>M_PI){
        th=th-2*M_PI;
    }
    if (th<-M_PI){
        th=th+2*M_PI;
    }
    actual.x=x;
    actual.y=y;
    actual.th=th;
    return actual;
}

int main(int argc, char** argv)
{
  
  ros::init(argc, argv, "drivetraj");
  ros::NodeHandle nh;


  ros::Rate rate(10.0);


  a.reserve(10000); 
  a.clear();
  t.reserve(10000); 
  t.clear();
  SubscriberTWD subTWD(nh);
 	//ros::Subscriber pathtwd_sub = nh.subscribe<nodelet_path_smoothing::OutputTraj>("/output_path/SmoothedPathPub",1, pathCallback);
  //printf("prosao pathtwd_sub");
  //ros::Subscriber sub = nh.subscribe("odom", 1000, SubscriberTWD::chatterCallback);
  //ros::Duration(5).sleep();

  while (nh.ok()) {
      ros::spinOnce(); 

  //drawing in rviz	  
      // if(trackingON == 1)
      // {
         //subTWD.kanayamaTracking();	
      // }
    
      rate.sleep();
    }
  return 0;
}


void SubscriberTWD::kanayamaTracking(){
// driving limitations for p3dx

double aradmax=0.1;    // m/s2
double alinmax=0.5;    // m/s2
double dt=0.1;
double amax=0.5;
double alfamax=1.75; //double alfamax=100*M_PI/180;
double vmax=0.75;
 double wmax = 1.75; //double wmax=100*M_PI/180;

// double vrob0=0.07;
//double vrob1=0.07; //intial and final velocity




        //tracking
        double xc,yc,thc,vc,wc;
        xc=a[0].x;
        yc=a[0].y;
        thc=a[0].th;
        if (init==0){
          vc=0.;//pocetna brzina
          wc=0;
        }else{
          vc = vrob0;
          wc = wrob0;
        }
        //ogranicenja brzina i akceleracija!!!!!
        //double dt=0.1;
        //double amax=0.5;
     //   double alfamax=1.75; //double alfamax=100*M_PI/180;
     //   double vmax=0.75;
      //  double wmax=1.75; //double wmax=100*M_PI/180;
        //goal coordinates
        T_point G=a.back();
        T_point actual;
        double xG=G.x-0.1;
        double yG=G.y;
        double thG=G.th;
        if (thG>M_PI){
            thG=thG-2*M_PI;
        }
        if (thG<-M_PI){
            thG=thG+2*M_PI;
        }
        // kanayama: some coefficient
        double k_x = 0.25;
        double k_y = 0.25;
        double k_theta = 5 * sqrt(k_y);
        double ex,ey,eth;
        double vs,ws,vt,wt,xt,yt,tht;
        double alfac,ac;

        int kk=0;
        int jj=-1;
        int krug = 0;


  FILE *logfile;
  logfile = fopen("odvozena","w");
  fclose(logfile);

	double tfx, tfy, yaw;
  double pitch, roll;
	//ros::NodeHandle n;
	ros::Rate rate(10.0);

  //vel_pub=nh_.advertise<geometry_msgs::Twist>("robot_0/cmd_vel", 1); //for multirobots
  vel_pub=nh_.advertise<geometry_msgs::Twist>("cmd_vel", 1);

  present = ros::Time::now();
  double duration;

  while (nh_.ok()) {
    ros::spinOnce(); 
	  try{
//        tf_listener.lookupTransform("map", "Pioneer3DX/base_link", ros::Time(0), transform);//robot_0/
        tf_listener.lookupTransform("map", "base_link", ros::Time(0), transform);//robot_0/
	  }
	  catch (tf::TransformException ex){
		  ROS_ERROR("%s",ex.what());
	  }
    transform.getBasis().getRPY(roll, pitch, yaw);
	  tfx=transform.getOrigin().x();
	  tfy=transform.getOrigin().y();
	  printf("\n map to base_link: x [%f], y [%f] and th [%f] deg\n", tfx, tfy, yaw*180/M_PI);

//tracking
  duration = (ros::Time::now() - present).toSec();
  printf("duration=%f\n",duration);
  printf("kk=%d\n",kk);
  printf("(xG,yG,thG)=(%f,%f,%f), current (xc,yc,thc)=(%f,%f,%f)\n",xG,yG,thG,xc,yc,thc);

        
          if ((fabs(xc-xG)>0.08) || (fabs(yc-yG)>0.08) || (std::min(fabs(thc-thG),std::min(fabs(thc-thG-2*M_PI),fabs(thc-thG+2*M_PI)))>25*M_PI/180) || (fabs(vc)>amax*dt) || (fabs(wc)>alfamax*dt)){
            //if((fabs(xc-xG)>0.08) || (fabs(yc-yG)>0.08)){ 
            jj=jj+1;
            while ((kk+1<a.size()) && (duration>=t[kk+1])){
                kk=kk+1;
            }
            vt=a[kk].v;
            wt=a[kk].w;
            xt=a[kk].x;
            yt=a[kk].y;
            tht=a[kk].th;
            ex=(xt-xc)*cos(thc)+(yt-yc)*sin(thc);
            ey=-(xt-xc)*sin(thc)+(yt-yc)*cos(thc);
            eth=tht-thc;
            if (vt<0) {//kljucan dio za rikverc
                eth=thc-tht;
            }
            if (eth>M_PI){
                eth=eth-2*M_PI;
            }
            if (eth<-M_PI){
                eth=eth+2*M_PI;
            }
//                corrected control
            ws = wt + vt * (k_y * ey + k_theta * sin(eth));
            alfac=alfamax*boost::math::sign(ws-wc);
            if (fabs((ws-wc)/dt)<fabs(alfac)){
                alfac=(ws-wc)/dt;
            }
            wc=wc+alfac*dt;
            if (wc>wmax){
                wc=wmax;
            }
            if (wc<-wmax){
                wc=-wmax;
            }
//                %corrected control
            vs = vt * cos(eth) + k_x * ex;
            ac=amax*boost::math::sign(vs-vc);
            if (fabs((vs-vc)/dt)<fabs(ac)){
                ac=(vs-vc)/dt;
            }
            printf("ac=%f vs=%f vc=%f\n",ac,vs,vc);
            vc=vc+ac*dt;
            if (vc>vmax){
                vc=vmax;
            }
            if (vc<-1*vmax){
                vc=-1*vmax;
            }

        //}else{
          if(patroling == 0){
            if((fabs(xc-xG)<0.1) && (fabs(yc-yG)<0.1)){
            while(fabs(vc)>amax*dt){
              ac=amax*boost::math::sign(vs-vc);
              vc=vc+ac*dt;
            }
            logfile = fopen("odvozena","a");
            fprintf(logfile,"%f %f %f %f %f %f %f %f\n", tfx, tfy, yaw*180/M_PI,vc,wc,odom_v,odom_w,duration);
            fclose(logfile);
            break;
          }
        }
            actual.x=xc;
            actual.y=yc;
            actual.v=vc;
            actual.th=thc;
            actual.w=wc;
            ref.push_back(actual);
            actual=kinmod(ref);
            //po modelu
            xc=actual.x;
            yc=actual.y;
            thc=actual.th;
            //po stvarnom gibanju
            xc=tfx;
            yc=tfy;
            thc=yaw;
            if (init==1){
              vrob0 = vc;
              wrob0 = wc;
            }
        }
          



    
    vel.linear.x = vc;
    vel.linear.y = 0.;
    vel.angular.z = wc;
    printf("setpoint: (%f,%f)\n",vc,wc);
    logfile = fopen("odvozena","a");
	  fprintf(logfile,"%f %f %f %f %f %f %f %f\n", tfx, tfy, yaw*180/M_PI,vc,wc,odom_v,odom_w,duration);
//	  fprintf(logfile,"%f %f %f %f %f %f %f %f\n", xc, yc, thc,vc,wc,odom_v,odom_w,duration);
    fclose(logfile);
    vel_pub.publish(vel);
    
	  rate.sleep();
    
  }
    
} 
    
void SubscriberTWD::lapsCallback(const nodelet_talker::PatrolingConstPtr& Msg){
  laps = Msg->lapsPatroling;
  ROS_INFO("Number of laps for patroling: %d", laps);
}


void SubscriberTWD::chatterCallback(const nav_msgs::Odometry::ConstPtr& msg)
{
  //ROS_INFO("Seq: [%d]", msg->header.seq);
  //ROS_INFO("Position-> x: [%f], y: [%f], z: [%f]", msg->pose.pose.position.x,msg->pose.pose.position.y, msg->pose.pose.position.z);
  //ROS_INFO("Orientation-> x: [%f], y: [%f], z: [%f], w: [%f]", msg->pose.pose.orientation.x, msg->pose.pose.orientation.y, msg->pose.pose.orientation.z, msg->pose.pose.orientation.w);
  //ROS_INFO("Vel-> Linear: [%f], Angular: [%f]", msg->twist.twist.linear.x,msg->twist.twist.angular.z);
  odom_v=msg->twist.twist.linear.x;
  odom_w=msg->twist.twist.angular.z;
  }

// citanje smoothing path podataka
void SubscriberTWD::pathCallback(const nodelet_path_smoothing::OutputTrajConstPtr& pMsg)
{
    
  a.clear();
  t.clear();
  //ROS_INFO("CISTIMO SVEEEEEE");
  // geometry_msgs::Point p; 
  //geometry_msgs::Twist vel;
  ROS_INFO("Velicina pMsg->points %d: , velicina pMsg->velocities %d ", pMsg->points.size(), pMsg->velocities.size());

  //for (int j=0; j < laps; j++){ // DODATKA za PATROLING nek ide N krugova
    for (int i=0; i < pMsg->points.size(); i++){
      //t.push_back(pMsg->t[i]);
      tr.x=pMsg->points[i].x;
      tr.y=pMsg->points[i].y;
      tr.th = pMsg->th[i];
      tr.v = pMsg->velocities[i].linear.x;
      tr.w = pMsg->velocities[i].angular.z;
      tr.curv = pMsg->curv[i];
      a.push_back(tr);
    }
    ROS_INFO("Velicina aaaaaaaaaaaaaaaaaaaaa %d ", a.size());
  //}


//ucitavanje iz acado spremljenih trajektorija
  FILE *trajfile;
  FILE *logdat;
  //char rdLine[36]="";
  int ret;
 //double f1;
  //double f2, disp, dist, accel, angAccel, curv, dCurv;
 //T_point tr;
 //std::vector<T_point> a;
  std::vector<T_point> ref;

 //std::vector<double> t;
  std::vector<double> theta;
  std::vector<double> v;
  std::vector<double> curv;
  std::vector<double> tt_;
  std::vector<double> uu;
  std::vector<double> xx;
  std::vector<double> yy;
  std::vector<double> xxd;
  std::vector<double> yyd;
  std::vector<double> xxdd;
  std::vector<double> yydd;
  std::vector<double> ds;
  std::vector<double> ddt;
  std::vector<double> kkk;
  std::vector<double> kkabs;
  std::vector<double> TP;
  std::vector<std::vector<double> > maksv;


  ref.reserve(10000); 
  ref.clear();
  uu.reserve(100000); 
  uu.clear();
  TP.reserve(10000); 
  TP.clear();
  maksv.reserve(10000); 
  maksv.clear();

// // driving limitations for husky
// double aradmax=6;    // m/s2
// double alinmax=3;    // m/s2
// double dt=0.1;
// double amax=3;
// double alfamax=1.75; //double alfamax=100*M_PI/180;
// double vmax=1;
// double wmax = 1.75; //double wmax=100*M_PI/180;

// driving limitations for p3dx
double aradmax=0.1;    // m/s2
double alinmax=0.5;    // m/s2
double dt=0.1;
       // double amax=0.5;
double alfamax=1.75; //double alfamax=100*M_PI/180;
double vmax=0.75;
 double wmax = 1.75; //double wmax=100*M_PI/180;


double vrob1=0.07; //intial and final velocity

double resolucija = a.size()-1;
double umin=0;
double umax=10;
double du, diffx, diffy, difftheta;

if (init == 0){
    vrob0 = 0.07;
    init = 1;
  // }else{
  //   vrob0 = odom_v;
  //   	printf("\nI heard: v [%f] \n", vrob0);
}

for (float i=umin; i<umax; i=i+(umax/resolucija)){
  uu.push_back(i);
  //printf("racunamo uu %f \n", i);
}
du = (float)(umax-umin)/resolucija;
printf("rezolucija = %d, uu = %d, du = %lf \n", resolucija, uu.size(), du);

for (int i = 0; i< resolucija; i++){
  xx.push_back(a[i].x);
  yy.push_back(a[i].y);
}
printf("size xx= %d, size yy=%d \n", xx.size(), yy.size());
for (int i=0; i< xx.size()-1; i++){
  diffx = (xx[i+1] - xx[i]);
  diffy = (yy[i+1] - yy[i]);
  difftheta = atan2(diffy , diffx);
  theta.push_back(difftheta);
  //printf("dx %f, dy %f", diffx,diffy);
  //theta.push_back(atan2(a[i].y-a[i-1].y,a[i].x-a[i-1].x));
  //theta.push_back(a[i].th);
  a[i].th = theta[i];
}

// ispisivanje svih vrijednosti za maksv
//  for (int i=0; i< theta.size(); i++){
//     printf("%f", theta[i]);
// }  

 // better derivatives (even better would be if spline of seccond order is fitted to the data)
 //printf("racunamo derivacije \n");
  xxd.push_back((-xx[0]+xx[2])/(2*du));
  yyd.push_back((-yy[0]+yy[2])/(2*du));
 for (int i=0; i<xx.size()-2; i++){
   xxd.push_back((-xx[i]+xx[i+2])/(2*du));
   yyd.push_back((-yy[i]+yy[i+2])/(2*du));
 }
 xxd.push_back(xxd[xxd.size()]);
 yyd.push_back(yyd[yyd.size()]);
 printf("size xxd= %d, size yyd=%d \n", xxd.size(), yyd.size());

 //printf("racunamo 2. derivacije \n");
 xxdd.push_back((-xxd[0]+xxd[2])/(2*du));
 yydd.push_back((-yyd[0]+yyd[2])/(2*du));
 for (int i=0; i < xxd.size()-2; i++){
   xxdd.push_back((-xxd[i]+xxd[i+2])/(2*du));
   yydd.push_back((-yyd[i]+yyd[i+2])/(2*du));
 }
 xxdd.push_back(xxdd[xxdd.size()]);
 yydd.push_back(yydd[yydd.size()]);
printf("size xxdd= %d, size yydd=%d \n", xxdd.size(), yydd.size());

printf("racunamo ds i kk \n");
// distance between samples according u
for (int i=0; i < xxd.size(); i++){
  ds.push_back((sqrt(xxd[i]*xxd[i] + yyd[i]* yyd[i])) * du); 
  //kkk.push_back((xxd[i]*yydd[i]-yyd[i]*xxdd[i]) / (pow((xxd[i]*xxd[i] + yyd[i]* yyd[i]),1.5)));
  kkabs.push_back(fabs(a[i].curv));
}
printf("size ds= %d, size kk=%d \n", ds.size(), kkabs.size());
 
// turn points

// add request for start velocity
TP.push_back(1);
for (int i=1; i< uu.size()-1; i++){
  if(kkabs[i] > kkabs[i-1] && kkabs[i] > kkabs[i+1])
    TP.push_back(i);   // index of TPi
}
// add request for start and end velocity
 TP.push_back(resolucija + 1);
printf("size of TP = %d, value 1: %f, value 2: %f \n", TP.size(), TP[0], TP[1]);

for (int i=0; i< TP.size(); i++){
  for(int j=0; j< resolucija+1; j++){
    maksv[i].push_back(resolucija);
  }
}

for (int i=0; i< TP.size(); i++){
  maksv[i][TP[i]] = sqrt(aradmax/kkabs[TP[i]]);
  // new condition for vmax
   if(maksv[i][TP[i]] > vmax){
    maksv[i][TP[i]] = vmax;
   }
   // new condition for wmax
   if((maksv[i][TP[i]]*kkabs[TP[i]]) > wmax){
    maksv[i][TP[i]] = wmax/kkabs[TP[i]];
   }
   
  // request for starting velocities
  if(i==0){
    if(maksv[i][TP[i]] < vrob0){
      printf("solution does not exist");
      double T=100; //solution does not exist
      break;
    }else
      {
        maksv[i][TP[i]] = vrob0;
      }
  }else if (i == TP.size())
  {
    if(maksv[i][TP[i]] < vrob1){
      printf("solution does not exist");
      double T=100; //solution does not exist
      break;
    }else
      {
        maksv[i][TP[i]] = vrob1;
      }
  }
// ispisivanje svih vrijednosti za maksv
//  for (int i=0; i< TP.size(); i++){
//   for(int j=0; j< resolucija+1; j++){
//     printf("%f", maksv[i][j]);
//   }
//   printf("\n");
// }  

  double goF=1; 
  double goB=1;
  for (int j=0; j< resolucija+1; j++){
    // calculate back from TP
    if (TP[i]-j-1 >= 0 && goB){
      double arad = pow(maksv[i][TP[i]-j],2) * kkabs[TP[i]-j];
      if (arad > aradmax){
        arad = aradmax;
      }
      double alin = alinmax * sqrt(1-(arad/aradmax*arad/aradmax));
      if (j==0){
        alin = 0;
      }
      maksv[i][TP[i]-j-1] = sqrt(2*alin*ds[TP[i]-j] + pow(maksv[i][TP[i]-j],2)); //v^2=(2as)+vi^2
      if (maksv[i][TP[i]-j-1] > vmax){
        maksv[i][TP[i]-j-1] = vmax;
      }
    }
    // calculate forward from TP
    if (TP[i]+j+1 <= (resolucija+1) && goF){
      double arad = pow(maksv[i][TP[i]+j],2) * kkabs[TP[i]+j];
      if (arad > aradmax){
        arad = aradmax;
      }
      double alin = alinmax * sqrt(1-(arad/aradmax*arad/aradmax));
      if (j==0){
        alin = 0;
      }
      maksv[i][TP[i]+j+1] = sqrt(2*alin*ds[TP[i]+j] + pow(maksv[i][TP[i]+j],2)); //v^2=(2as)+vi^2
      if (maksv[i][TP[i]+j+1] > vmax){
        maksv[i][TP[i]+j+1] = vmax;
      }
    }else {
      if (!goB){  // finish for loop if both goB and goF are false  
        break;
      }
    }
  }
}
  printf("racunamo brzinu\n");
// //v=min(maksv); //v je vektor koji sadzi najmanju vrijednost svakog stupca zato su i,j okrenuti
double min;
for(int j=0; j< resolucija; j++){
  min = maksv[0][j];
  for (int i=1; i<  TP.size(); i++){

    if (min > maksv[i][j]){
      min = maksv[i][j];
    }
  }
  v.push_back( min);
  a[j].v = min; 
}
printf("size of v = %d, value of v = %f: \n", v.size(), a[0].v);
// odredivanje w
for (int i=0; i < v.size(); i++)
{
  a[i].w = a[i].v * a[i].curv;
  //printf("w = %f \n", a[i].w);
}
//odredivanje dt
for (int i=0; i< ds.size(); i++){
  //dt[i] = ds[i]/v;
  if (i == ds.size()-1){
    v[i+1] = 0;
  }
  ddt.push_back(2*ds[i]/(v[i+1]+v[i]));
}
//printf("size of w = %d, ddt = %d: \n", a.size(), ddt.size());
double T=0;
double D=0;
for (int i=0; i < resolucija; i++)
{
  T = T + ddt[i+1];
}
for (int i=0; i < ds.size(); i++)
{
  D = D + ds[i];
}
printf("time T: %f, length D: %f", T,D);
// tt
tt_.push_back(0);
t.push_back(0);
for (int i = 1; i < resolucija; i++){
  tt_.push_back(tt_[i-1] + ddt[i]);
  t.push_back(tt_[i-1] + ddt[i]);
  //a[i].t = tt_;
}
printf("size of tt_ = %d: \n", tt_.size());

// SPREMANJE PODATAKA  
  if ( (trajfile = fopen("smoothedtraj23.txt","w")) == NULL ){
				 printf("Error! trajfile couldn't be opened.");
	}else {
        //logdat = fopen("smoothedtraj1","w");
        for (int i=0; i< a.size()-1; i++){
            fprintf(trajfile,"%f %f %f %f %f %f %f\n", tt_[i], a[i].x, a[i].y, a[i].th, a[i].v, a[i].w, a[i].curv);
        }
        }
		fclose(trajfile);

  printf("\nsize t=%d, size a=%d\n",t.size(),a.size());
  trackingON = 1;


  SubscriberTWD::kanayamaTracking();	

}
